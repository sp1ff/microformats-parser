use assert_json_diff::assert_json_eq;

// This aims to cache the issue at
// https://gitlab.com/maxburon/microformats-parser/-/issues/7#note_951502062
#[test]
fn issue_7() -> Result<(), crate::Error> {
    crate::test::enable_logging();

    let html = r#"
<article class="h-entry">
  <header class="metadata">
    <div>
      <span>
        <a class="u-url u-uid" href="https://fireburn.ru/posts/est-eligendi-deleniti">
          <time class="dt-published" datetime="2022-05-16T20:41:45.032834285+03:00">Mon May 16 20:41:45 2022</time>
        </a>
      </span>
    </div>
  </header>
  <main class="e-content">
    <p>Aut repellat tempora. Incidunt dolorum earum iste. Quis velit necessitatibus numquam et quaerat recusandae. Eius harum maxime qui.</p>
  </main>
  <footer class="webinteractions">
    <ul class="counters">
      <li><span class="icon">❤️</span><span class="counter">0</span></li>
      <li><span class="icon">💬</span><span class="counter">0</span></li>
      <li><span class="icon">🔄</span><span class="counter">0</span></li>
      <li><span class="icon">🔖</span><span class="counter">0</span></li>
    </ul>
  </footer>
</article>
    "#;

    let json_str = r#"
{
    "items": [
        {
            "type": ["h-entry"], 
            "properties": {
                "uid": ["https://fireburn.ru/posts/est-eligendi-deleniti"],
                "url": ["https://fireburn.ru/posts/est-eligendi-deleniti"], 
                "published": ["2022-05-16T20:41:45.032834285+0300"], 
                "content": [{
                    "value": "Aut repellat tempora. Incidunt dolorum earum iste. Quis velit necessitatibus numquam et quaerat recusandae. Eius harum maxime qui.", 
                    "html": "<p>Aut repellat tempora. Incidunt dolorum earum iste. Quis velit necessitatibus numquam et quaerat recusandae. Eius harum maxime qui.</p>"
                }]
            }
        }
    ]
}
    "#;
    use std::ops::Deref;

    let expected_json_mf2_item = serde_json::from_str(json_str.trim())
        .map(|document: crate::types::Document| document.items[0].clone())
        .map_err(crate::Error::JSON)
        .and_then(|d| serde_json::to_value(d.borrow().deref()).map_err(crate::Error::JSON))?;

    let parsed_html_mf2_json = crate::from_html(
        html,
        "http://fireburn.ru/posts/est-eligendi-deleniti"
            .parse()
            .unwrap(),
    )
    .map(|document| document.items[0].clone())
    .and_then(|d| serde_json::to_value(d.borrow().deref()).map_err(crate::Error::JSON))?;

    assert_json_eq!(expected_json_mf2_item, parsed_html_mf2_json);

    Ok(())
}
