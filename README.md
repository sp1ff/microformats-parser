# Microformats parser

This project provides a [microformats](http://microformats.org/) parser in Rust.

## Usage

Installation:
```
cargo build
```

Parse the microformats from a URL:
```
curl <URL> | cargo run
```
